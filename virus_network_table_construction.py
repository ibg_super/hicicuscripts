infile1 = open("IC9_clustering.mcl", "r")
infile2 = open("IC9_DemoVir_viralverify_v.txt", "r")
infile3 = open("IC9_DemoVir_viralverify_v_uns.txt", "r")
infile4 = open("IC9_good.csv", "r")
mag_info_file = open("IC9_bin.txt", "r")
cont_mag = dict()
bins = dict()
n = 1
for line in infile1:
    words = list(line.split())
    for word in words:
        cont_mag[word] = n
    bins[n] = set(words)
    n += 1

virus_dict = dict()
ver_q = set()
n = 1
for line in infile2:
    if n == 1:
        n += 1
    else:
        words = line.split()
        virus_dict[words[0]] = list(words[1:])
        if float(words[2]) > 50:
            ver_q.add(words[0])
n = 1
for line in infile3:
    if n == 1:
        n += 1
    else:
        words = line.split()
        virus_dict[words[0]] = list(words[1:])
        if float(words[2]) > 50:
            ver_q.add(words[0])

mag = set(cont_mag.keys())
viral = set(virus_dict.keys())
link_bin = dict()
n = 0
for line in infile4:
   # if n % 1000 == 0:
        #print(n)
    n += 1
    words = line.split(',')
    if words[0] in mag:
        if words[1] in viral:
            if words[1] not in link_bin.keys():
                link_bin[words[1]] = dict()
                link_bin[words[1]][cont_mag[words[0]]] = float(words[2])
            elif cont_mag[words[0]] not in link_bin[words[1]].keys():
                link_bin[words[1]][cont_mag[words[0]]] = float(words[2])
            else:
                link_bin[words[1]][cont_mag[words[0]]] = max(float(words[2]), link_bin[words[1]][cont_mag[words[0]]])
    if words[1] in mag:
        if words[0] in viral:
            if words[0] not in link_bin.keys():
                link_bin[words[0]] = dict()
                link_bin[words[0]][cont_mag[words[1]]] = float(words[2])
            elif cont_mag[words[1]] not in link_bin[words[0]].keys():
                link_bin[words[0]][cont_mag[words[1]]] = float(words[2])
            else:
                link_bin[words[0]][cont_mag[words[1]]] = max(float(words[2]), link_bin[words[0]][cont_mag[words[1]]])


bin_info = dict()
good_bin = set()
for line in mag_info_file:
    words = line.split()
    num_bin = int(words[0].strip("CL"))
    tax = words[6].split(";")
    family = tax[4]
    bin_info[num_bin] = (float(words[3]), float(words[4]), family)
    if float(words[3]) >= 80 and float(words[4]) <= 5:
        good_bin.add(num_bin)

outfile = open("IC9_vir_mag_linked_info.txt", "w")
outfile_r = open("IC9_vir_mag_linked_for_R.txt", "w")

num_bin = 0
num_vir_connect = 0
num_vir_many = 0
vir_f_mag_f = dict()
for key in link_bin.keys():
   # if key in ("k141_179", "k141_69571", "k141_69650", "k141_37431", "k141_59782", "k141_88038", "k141_88345", "k141_52917", "k141_21758"):
   #     print(key, link_bin[key])
    num_bin = 0
    for bin in link_bin[key].keys():
        if link_bin[key][bin] >= 0.6 and bin in good_bin:
            num_bin += 1
            print(key, bin, link_bin[key][bin], virus_dict[key][0], virus_dict[key][1], virus_dict[key][2], virus_dict[key][3], *bin_info[bin], file=outfile)
            if bin_info[bin][2] in vir_f_mag_f.keys():
                if virus_dict[key][2] == "Unassigned":
                    virus_dict[key][2] = virus_dict[key][0]

                if virus_dict[key][2] in vir_f_mag_f[bin_info[bin][2]].keys():
                    vir_f_mag_f[bin_info[bin][2]][virus_dict[key][2]] += 1
                else:
                    vir_f_mag_f[bin_info[bin][2]][virus_dict[key][2]] = 1
            else:
                if virus_dict[key][2] == "Unassigned":
                    virus_dict[key][2] = virus_dict[key][0]
                vir_f_mag_f[bin_info[bin][2]] = dict()
                vir_f_mag_f[bin_info[bin][2]][virus_dict[key][2]] = 1

    if num_bin > 0:
        num_vir_connect += 1
    if num_bin > 1:
        num_vir_many += 1
print('many', num_vir_many, 'only one ', num_vir_connect - num_vir_many)

for mag in vir_f_mag_f.keys():
    for vir in vir_f_mag_f[mag].keys():
        print(mag, vir, vir_f_mag_f[mag][vir], file=outfile_r)

print(len(virus_dict.keys()))
