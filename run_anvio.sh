#!/bin/bash

N_THREADS_MULTI=

MAG_FASTA=
MAG_CONTIG_DB=
GENOMES_NAME=
NUM_GENOMES=


#py36
#anvi-script-FASTA-to-contigs-db IC4_CL002.fa
#anvi-script-reformat-fasta IC4_CL002_chromosome.fasta -o IC4_CL002.fasta -l 0 --simplify-names

anvi-gen-contigs-database -f $MAG_FASTA -o $MAG_CONTIG_DB -T $N_THREADS_MULTI
anvi-run-hmms -T $N_THREADS_MULTI -c $MAG_CONTIG_DB
#anvi-setup-ncbi-cogs
anvi-run-ncbi-cogs --num-threads $N_THREADS_MULTI -c $MAG_CONTIG_DB --search-with blastp
#anvi-run-kegg-kofams -c CONTIGS_DB -T 8 

anvi-gen-genomes-storage -e external-genomes.txt -o $GENOMES_NAME-GENOMES.db

anvi-pan-genome -g $GENOMES_NAME-GENOMES.db -n $GENOMES_NAME --output-dir $GENOMES_NAME --num-threads $N_THREADS_MULTI --use-ncbi-blast --mcl-inflation 10

anvi-display-pan -p $GENOMES_NAME/$GENOMES_NAME-PAN.db -g $GENOMES_NAME-GENOMES.db

anvi-get-sequences-for-gene-clusters -g $GENOMES_NAME-GENOMES.db -p $GENOMES_NAME/$GENOMES_NAME-PAN.db --max-num-genes-from-each-genome 1 --min-num-genomes-gene-cluster-occurs $NUM_GENOMES --concatenate-gene-clusters -o concatenated-proteins.fa --max-functional-homogeneity-index 0.9 --min-geometric-homogeneity-index 1

anvi-gen-phylogenomic-tree -f concatenated-proteins.fa -o phylogenomic-tree.txt

anvi-import-misc-data -p $GENOMES_NAME/$GENOMES_NAME-PAN.db -t layer_orders layers_order.txt

anvi-display-pan -p $GENOMES_NAME/$GENOMES_NAME-PAN.db -g $GENOMES_NAME-GENOMES.db

anvi-summarize -p $GENOMES_NAME/$GENOMES_NAME-PAN.db -g $GENOMES_NAME-GENOMES.db -o $GENOMES_NAME-SUMMARY -C default

